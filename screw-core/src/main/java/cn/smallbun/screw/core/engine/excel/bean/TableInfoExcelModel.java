package cn.smallbun.screw.core.engine.excel.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.excel.annotation.write.style.HeadStyle;
import lombok.Builder;
import lombok.Data;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

@Data
@Builder
@HeadFontStyle(fontHeightInPoints = 12)
@HeadRowHeight(20)
@HeadStyle(horizontalAlignment = HorizontalAlignment.LEFT)
@ContentStyle(horizontalAlignment = HorizontalAlignment.LEFT)
public class TableInfoExcelModel {

    @ExcelProperty("表名")
    private String tableName;

    @ExcelProperty("备注")
    private String remark;

}
